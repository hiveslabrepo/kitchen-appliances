<style>

    .gallery
{
    display: inline-block;
    margin-top: 20px;
}
    
</style>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

<div class="container">
	<div class="row">
		<div class='list-group gallery'>
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="images/troli/perforated%20cutlery%20basket-1.jpg">
                    <img style="height: 250px;
        width: 320px;" class="img-responsive" alt="" src="images/troli/perforated%20cutlery%20basket-1.jpg"/>
                    <div class='text-right'>
                        <small class='text-muted'></small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="images/troli/Plain%20Basket-1.jpg">
                    <img style="height: 250px;
        width: 320px;" class="img-responsive" alt="" src="images/troli/Plain%20Basket-1.jpg" />
                    <div class='text-right'>
                        <small class='text-muted'></small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="images/troli/Plain%20Basket.jpg">
                    <img style="height: 250px;
        width: 320px;" class="img-responsive" alt="" src="images/troli/Plain%20Basket.jpg" />
                    <div class='text-right'>
                        <small class='text-muted'></small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="images/troli/Plain-Partition%20Basket.jpg">
                    <img style="height: 250px;
        width: 320px;" class="img-responsive" alt="" src="images/troli/Plain-Partition%20Basket.jpg" />
                    <div class='text-right'>
                        <small class='text-muted'></small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="images/troli/Plate%20Basket.jpg">
                    <img style="height: 250px;
        width: 320px;" class="img-responsive" alt="" src="images/troli/Plate%20Basket.jpg" />
                    <div class='text-right'>
                        <small class='text-muted'></small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
            <div class='col-sm-4 col-xs-6 col-md-3 col-lg-3'>
                <a class="thumbnail fancybox" rel="ligthbox" href="images/troli/Pull-out-basket.jpg">
                    <img style="height: 250px;
        width: 320px;x" class="img-responsive" alt="" src="images/troli/Pull-out-basket.jpg" />
                    <div class='text-right'>
                        <small class='text-muted'></small>
                    </div> <!-- text-right / end -->
                </a>
            </div> <!-- col-6 / end -->
        </div> <!-- list-group / end -->
	</div> <!-- row / end -->
</div> <!-- container / end -->

<script>

    $(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
   
  
    
</script>