<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Kitchen Wonders The Best KitchenMarket Designed By Hives Online India Pvt.Ltd.</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:title" content="Vide" />
<meta name="keywords" content="Kitchen Wonders The Best KitchenMarket Designed By Hives Online India Pvt.Ltd." />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- js -->
   <script src="js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smoth-scrolling -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
<!--- start-rate---->
<script src="js/jstarbox.js"></script>
	<link rel="stylesheet" href="css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
		<script type="text/javascript">
			jQuery(function() {
			jQuery('.starbox').each(function() {
				var starbox = jQuery(this);
					starbox.starbox({
					average: starbox.attr('data-start-value'),
					changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
					ghosting: starbox.hasClass('ghosting'),
					autoUpdateAverage: starbox.hasClass('autoupdate'),
					buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
					stars: starbox.attr('data-star-count') || 5
					}).bind('starbox-value-changed', function(event, value) {
					if(starbox.hasClass('random')) {
					var val = Math.random();
					starbox.next().text(' '+val);
					return val;
					} 
				})
			});
		});
		</script>
<!---//End-rate---->

</head>
<body>
<a href="offer.php"><img src="images/download.png" class="img-head" alt=""></a>
<div class="header">

		<div class="container">
			
			<div class="logo">
				<h1><a href="index.php">Kitchen Wonders<span>complete kitchen solutions</span></a></h1>
			</div>
			<div class="head-t">
				<ul class="card">
<!--
					<li><a href="wishlist.html" ><i class="fa fa-heart" aria-hidden="true"></i>Wishlist</a></li>
					<li><a href="login.html" ><i class="fa fa-user" aria-hidden="true"></i>Login</a></li>
					<li><a href="register.html" ><i class="fa fa-arrow-right" aria-hidden="true"></i>Register</a></li>
					<li><a href="about.html" ><i class="fa fa-file-text-o" aria-hidden="true"></i>Order History</a></li>
					<li><a href="shipping.html" ><i class="fa fa-ship" aria-hidden="true"></i>Shipping</a></li>
-->
				</ul>	
			</div>
			
			<div class="header-ri">
				<p style="color:red;">&nbsp;Arihant Agencies</p>	<br>
			     </div>

				<div class="nav-top">
					<nav class="navbar navbar-default">
					
					<div class="navbar-header nav_2">
						<button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						

					</div> 
					<div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
						<ul class="nav navbar-nav ">
							<li ><a href="index.php" class="hyper"><span>Home</span></a></li>	
							
							<li class="dropdown ">
								<a href="#" class="dropdown-toggle  hyper" data-toggle="dropdown" ><span>Category<b class="caret"></b></span></a>
								<ul class="dropdown-menu multi">
									<div class="row">
										<div class="col-sm-3">
											
			<li><a href="kitchen.php?name=HomeAppliance"><b>Home Appliance</b></a></li>
                                            <ul class="multi-column-dropdown">
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Electric Geyser</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Gas Geyser</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Water Purifire</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Iron</li>
										        <li><i class="fa fa-angle-right" aria-hidden="true"></i>Hot Plate</li>
											</ul>
										
										</div>
										<div class="col-sm-3">
            <li><a href="kitchen.php?name=KitchenAppliance"><b>Kitchen Appliance</b></a></li>
											<ul class="multi-column-dropdown">
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Gas Stove</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Kitchen Chimney</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Induction CookTop</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Pressure Cooker</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Hand Blender</li>
										        <li><i class="fa fa-angle-right" aria-hidden="true"></i>Mixer Grinder</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Food Processor</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Toaster</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Electric Kettle</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Cookware</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Roti Macker</li>
                                                
											</ul>
										
										</div>
										<div class="col-sm-3">
                    <li><a href="kitchen.php?name=KitchenTools"><b>Kitchen Tools</b></a></li>	
											<ul class="multi-column-dropdown">
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Knife</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Gas Tube</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Gas Lighter</li>
												<li><i class="fa fa-angle-right" aria-hidden="true"></i>Cylinder Trolly</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Gas stove leg set</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Gas Stove Acceseries</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Spatula</li>
                                                <li><i class="fa fa-angle-right" aria-hidden="true"></i>Wooden Spoons</li>
										
											</ul>
										</div>
<!--										<div class="col-sm-3 w3l">-->
<!--											<a href="kitchen.php"><img src="images/me.png" class="img-responsive" alt=""></a>-->
<!--										</div>-->
										<div class="clearfix"></div>
									</div>	
								</ul>
							</li>
                            <li class="active"><a href="offer.php" class="hyper"><span>Kitchen Trolleys & Furniture</span></a></li>
							<li><a href="about.php" class="hyper"><span>About Us</span></a></li>
							<li><a href="contact.php" class="hyper"><span>Contact Us</span></a></li>
						</ul>
					</div>
					</nav>
<!--
					 <div class="cart" >
					
						<span class="fa fa-shopping-cart my-cart-icon"><span class="badge badge-notify my-cart-badge"></span></span>
					</div>
-->
					<div class="clearfix"></div>
				</div>
					
				</div>			
</div>
  <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <div class="item active">
          <a href="#"><img class="first-slide" src="images/Kitchen-Interior-Wallpaper-9.jpg" alt="First slide" style="margin-left: 75px;"></a>
       
        </div>
        <div class="item">
          <a href="#"><img class="second-slide " src="images/Kitchen%20Design%20HD%203D%20Desktop%20Bacground%20Wallpaper%20Albums%20(9).jpg" alt="Second slide" style="margin-left: 75px;" ></a>
         
<!--
        </div>
        <div class="item">
          <a href="hold.html"><img class="third-slide " src="images/ba2.jpg" alt="Third slide"></a>
          
        </div>
-->
      </div>
    
    </div>
    </div>        
<!--content-->
    
<!--    -->
<div class="content-top offer-w3agile">
	<div class="container ">
		<div class="spec ">
			<h3>Kitchen Trolleys & Furniture</h3>
							<div class="ser-t">
					<b></b>
					<span><i></i></span>
					<b class="line"></b>
				</div>
		</div>
        <?php include('modal.php');?>
    </div>
</div>
    
					

<!--Footer-->
<div class="footer">
	<div class="container">
		<div class="col-md-9 footer-grid">
			<h3>About Us</h3>
			<p>We started making Modular Kitchens & Kitchen Furniture from 1999 & one of the oldest Firms offering this kind of service in PUNE. We have our own manufacturing Unit for Kitchen Trollies, Furniture & different types of Shutters.</p>
		</div>
		<div class="col-md-3 footer-grid ">
			<h3>Menu</h3>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="kitchen.php">Category</a></li>
				<li><a href="offer.php">Kitchen Trolleys &amp; Furniture</a></li>
				<li><a href="about.php">About Us</a></li> 
				<li><a href="contact.php">Contact</a></li>
			</ul>
		</div>
<!--
		<div class="col-md-3 footer-grid ">
			<h3>Customer Services</h3>
			<ul>
				<li><a href="#">Shipping</a></li>
				<li><a href="#">Terms & Conditions</a></li>
				<li><a href="faqs.html">Faqs</a></li>
				<li><a href="#">Contact</a></li>
				<li><a href="#">Online Shopping</a></li>						 
				 
			</ul>
		</div>
-->
<!--
		<div class="col-md-3 footer-grid">
			<h3>My Account</h3>
			<ul>
				<li><a href="login.html">Login</a></li>
				<li><a href="register.html">Register</a></li>
				<li><a href="wishlist.html">Wishlist</a></li>
				
			</ul>
		</div>
-->
		<div class="clearfix"></div>
			<div class="footer-bottom">
				<h2><a href="index.php">Kitchen Wonders<span>complete kitchen solutions</span></a></h2>
				<p class="fo-para"></p>
<!--
				<ul class="social-fo">
					<li><a href="#" class=" face"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#" class=" twi"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#" class=" pin"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
					<li><a href="#" class=" dri"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
				</ul>
-->
				<div class=" address">
					<div class="col-md-4 fo-grid1">
							<p><i class="fa fa-home" aria-hidden="true"></i>Shop no.1,Plot no.3A,NINA Scty,S.NO.148,4a+4b,Opposite Vanaz Factory,Near Federal Bank , Paud Road , Kothrud , Pune-38</p>
					</div>
					<div class="col-md-4 fo-grid1">
							<p>
                            For <br>Kitchen Appliances<br><i class="fa fa-phone" aria-hidden="true"></i>  020 25398781 / +91 9881201871<br>
                            Kitchen Trolleys and Furniture<br><i class="fa fa-phone" aria-hidden="true"></i> 9822196033</p>	
					</div>
					<div class="col-md-4 fo-grid1">
						<p><a href="mailto:info@example.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>info@example1.com</a></p>
					</div>
					<div class="clearfix"></div>
					
				</div>
			</div>
		<div class="copy-right">  
			<p> &copy; 2017 Kitchen Wonders . All Rights Reserved |<br> Designed by  <a href="http://www.hiveslab.com/"><br>Hives Online India Pvt.Ltd </a></p>
            <center><img src="images/whives.png" height="60px" width="60px"></center>
		</div>
	</div>
</div>
        
<!-- //footer-->

<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<!-- for bootstrap working -->
		<script src="js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<script type='text/javascript' src="js/jquery.mycart.js"></script>
  <script type="text/javascript">
  $(function () {

    var goToCartIcon = function($addTocartBtn){
      var $cartIcon = $(".my-cart-icon");
      var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
      $addTocartBtn.prepend($image);
      var position = $cartIcon.position();
      $image.animate({
        top: position.top,
        left: position.left
      }, 500 , "linear", function() {
        $image.remove();
      });
    }

    $('.my-cart-btn').myCart({
      classCartIcon: 'my-cart-icon',
      classCartBadge: 'my-cart-badge',
      affixCartIcon: true,
      checkoutCart: function(products) {
        $.each(products, function(){
          console.log(this);
        });
      },
      clickOnAddToCart: function($addTocart){
        goToCartIcon($addTocart);
      },
      getDiscountPrice: function(products) {
        var total = 0;
        $.each(products, function(){
          total += this.quantity * this.price;
        });
        return total * 1;
      }
    });

  });
  </script>
  
 <!-- product -->
			<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
								<div class="col-md-5 span-2">
											<div class="item">
												<img src="images/troli/perforated%20cutlery%20basket-1.jpg" class="img-responsive" alt="">
											</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
<!-- product -->
			<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
								<div class="col-md-5 span-2">
											<div class="item">
												<img src="images/troli/Plain%20Basket-1.jpg" class="img-responsive" alt="">
											</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
				<!-- product -->
			<div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
								<div class="col-md-5 span-2">
											<div class="item">
												<img src="images/of2.png" class="img-responsive" alt="">
											</div>
								</div>
								<div class="col-md-7 span-1 ">
									<h3>Kabuli Chana(1 kg)</h3>
									<p class="in-para"> There are many variations of passages of Lorem Ipsum.</p>
									<div class="price_single">
									  <span class="reducedfrom "><del>$3.00</del>$2.00</span>
									
									 <div class="clearfix"></div>
									</div>
									<h4 class="quick">Quick Overview:</h4>
									<p class="quick_desc"> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; es</p>
									 <div class="add-to">
										   <button class="btn btn-danger my-cart-btn my-cart-btn1 " data-id="3" data-name="Kabuli Chana" data-summary="summary 3" data-price="2.00" data-quantity="1" data-image="images/of2.png">Add to Cart</button>
										</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
        
            <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
								<div class="col-md-5 span-2">
											<div class="item">
												<img src="images/of.png" class="img-responsive" alt="">
											</div>
								</div>
								<div class="col-md-7 span-1 ">
									<h3>Moong(1 kg)</h3>
									<p class="in-para"> There are many variations of passages of Lorem Ipsum.</p>
									<div class="price_single">
									  <span class="reducedfrom "><del>$2.00</del>$1.50</span>
									
									 <div class="clearfix"></div>
									</div>
									<h4 class="quick">Quick Overview:</h4>
									<p class="quick_desc"> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; es</p>
									 <div class="add-to">
										   <button class="btn btn-danger my-cart-btn my-cart-btn1 " data-id="1" data-name="Moong" data-summary="summary 1" data-price="1.50" data-quantity="1" data-image="images/of.png">Add to Cart</button>
										</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
    
            <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
								<div class="col-md-5 span-2">
											<div class="item">
												<img src="images/of.png" class="img-responsive" alt="">
											</div>
								</div>
								<div class="col-md-7 span-1 ">
									<h3>Moong(1 kg)</h3>
									<p class="in-para"> There are many variations of passages of Lorem Ipsum.</p>
									<div class="price_single">
									  <span class="reducedfrom "><del>$2.00</del>$1.50</span>
									
									 <div class="clearfix"></div>
									</div>
									<h4 class="quick">Quick Overview:</h4>
									<p class="quick_desc"> Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; es</p>
									 <div class="add-to">
										   <button class="btn btn-danger my-cart-btn my-cart-btn1 " data-id="1" data-name="Moong" data-summary="summary 1" data-price="1.50" data-quantity="1" data-image="images/of.png">Add to Cart</button>
										</div>
								</div>
								<div class="clearfix"> </div>
							</div>
						</div>
					</div>
				</div>
				
</body>
</html>