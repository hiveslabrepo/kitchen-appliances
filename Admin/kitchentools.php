 <?php 
			//session_start();
			//echo $_SESSION['img'];
            include("config.php");
session_start();
if(isset($_SESSION['uid']))
{}
else
{
    echo "<script>window.location.href='../login.php';</script>";
}
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | KitchenTools</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  
  <?php
                  $cnt=0;
                  $str="SELECT * FROM message";
                $res=mysqli_query($conn,$str);
     $res1=mysqli_query($conn,$str);
    //$res2=mysqli_query($conn,$str);
                ?>
  <header class="main-header">
    <!-- Logo -->
    <a href="index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
            
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
            
                
              <span class="label label-success">
                  <?php
                  while($row=mysqli_fetch_array($res))
                  {
                      $cnt++;
                  }
                  echo $cnt; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php //echo $row['mid']; ?> messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <!-- start message -->
                      
                  
    
                    <?php 
                   
                        while($row1=mysqli_fetch_array($res1))
                            { 
                            //$cnt++; 
                           
                    ?>
                  <li>
                    <a href="read-mail.php?name=<?php echo $row1['mid'];?>">
                      <h4>
                        <?php echo $row1['mname'];?>
                        <small><i class="fa fa-clock-o"></i></small>
                      </h4>
                      <p><?php echo $row1['mmessage']; ?></p>
                    </a>
                  </li>
                                      <?php  
                            } 
                        ?>
                        <!-- end message -->
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
            
                   
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 0 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have No Task</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">

                  <!-- end task item -->
                    <!-- Task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../images/userdefault.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../images/userdefault.png" class="img-circle" alt="User Image">

                <p>
                  Admin - 
<!--                  <small>Member since Nov. 2012</small>-->
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <form action="" method="post">
                  <div class="pull-right" style="margin-right: 80px;">
                      <input type="submit" class="btn btn-primary" name="out" value="Sign Out">
                </div>
                </form>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../images/userdefault.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo "Kitchen Wonders"; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Add Product</span>
            <span class="pull-right-container">
            
            <small class="label pull-right bg-green">new</small></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="addhomeappli.php"><i class="fa fa-circle-o"></i>Home Appliance</a></li>
            <li><a href="kitchenappli.php"><i class="fa fa-circle-o"></i>Kitchen Appliance</a></li>
            <li class="active"><a href="kitchentools.php"><i class="fa fa-circle-o"></i>Kitchen Tools</a></li>
              <li><a href="sliderimage.php"><i class="fa fa-circle-o"></i>Image Home Slider</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="paymentdetails.php">
            <i class="fa fa-money"></i>
            <span>Transaction Details</span>
          </a>
        </li>
        <li class="treeview">
          <a href="tables.php">
            <i class="fa fa-th"></i> <span>Tables</span>
          </a>
        </li>
        <li>
          <a href="pages/mailbox/mailbox.php">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
        <li><a href="#">New</a></li>
        <li>Add Kitchen Tools</li>
      </ol>
        <div class="container">
            <center><h1>Add Kitchen Tools</h1></center>
            <hr>
            <div class="row">      
              <div class="col-md-12 personal-info">   
                <form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
<!--
                <div class="form-group">
                  <label class="col-lg-5 control-label"> style="text-align:left"Enter Product ID :</label>
                    <div class="col-lg-5">
                      <input class="form-control" placeholder="Enter Product Id" name="pid" type="text">
                    </div>
                  </div>
-->
                  <div class="form-group">
                    <label class="col-lg-5 control-label">Enter Product Name :</label>
                    <div class="col-lg-5">
                      <input class="form-control" type="text" name="pnm" placeholder="Enter Product Name" required>
                    </div>
                    </div>
				  <div class="form-group">
                  <label class="col-lg-5 control-label"><!-- style="text-align:left"-->MRP:</label>
                    <div class="col-lg-5">
                      <input class="form-control" placeholder="Enter MRP" name="pmrp" type="text" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Offer Price :</label>
                    <div class="col-lg-5">
                      <input class="form-control" placeholder="Enter Offer Price" name="pop" type="text" required>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Description :</label>
                    <div class="col-lg-5">
                      <input class="form-control" placeholder="Enter Description of product" name="dec" type="textarea" required>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Uploade Image:</label>
                    <div class="col-lg-5">
                      <input type="file" name="fileinput" required>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                      <input class="btn btn-primary" type="submit" value="Submit" name="submit" >
                      <span></span>
                      <input class="btn btn-default" type="reset" value="Cancel" name="reset" >
                    </div>
                  </div>
                </form>
            </div>
        
    </div>
</div>
<hr>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
<!--      <b>Version</b> 2.3.6-->
    </div>
  <strong>Copyright &copy; 2017-2018 <a href="http://Hiveslab.com" target="_blank">Hives Online India Pvt.Ltd</a>.</strong> All rights
    reserved.
  </footer>

  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
<!--mysqli_query($conn,"query");-->
<!--mysqli_fetch_array($result);-->
<?php
 if(isset($_POST['submit']))
 {
     //$pno=$_POST['pno'];
     $pname=$_POST['pnm'];
     $mrp=$_POST['pmrp'];
     $pop=$_POST['pop'];
     $dec=$_POST['dec'];
    $filename=$_FILES['fileinput']['name'];
	$filetype=$_FILES['fileinput']['type'];
	$filesize=$_FILES['fileinput']['size'];
	$filetemp=$_FILES['fileinput']['tmp_name'];
	
     move_uploaded_file($filetemp,"../images/$filename");
     $str="INSERT INTO kittools(tname,mrp,oprice,discription,img) VALUES('".$pname."',".$mrp.",".$pop.",'".$dec."','".$filename."')";
     if(mysqli_query($conn,$str))
     {
         echo"<script>alert('data added successfully');</script>";
     }
     else
     {
         echo"<script>alert('error');</script>";
     }
 }
?>
