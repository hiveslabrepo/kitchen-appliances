<?php
    //echo "<script>alert(".$_GET['name'].");</script>";
    include('config.php');
session_start();
if(isset($_SESSION['uid']))
{}
else
{
    echo"<script>window.location.href='../../../login.php';</script>";
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Read Mail</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- fullCalendar 2.2.5-->
  <link rel="stylesheet" href="../../plugins/fullcalendar/fullcalendar.min.css">
  <link rel="stylesheet" href="../../plugins/fullcalendar/fullcalendar.print.css" media="print">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../plugins/iCheck/flat/blue.css">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php
                  $cnt=0;
                  $str="SELECT * FROM message";
                $res=mysqli_query($conn,$str);
     $res1=mysqli_query($conn,$str);
    //$res2=mysqli_query($conn,$str);
                ?>
  <header class="main-header">
    <!-- Logo -->
    <a href="../../index.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
            
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
            
                
              <span class="label label-success">
                  <?php
                  while($row=mysqli_fetch_array($res))
                  {
                      $cnt++;
                  }
                  echo $cnt; ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php //echo $row['mid']; ?> messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <!-- start message -->
                      
                  
    
                    <?php 
                   
                        while($row1=mysqli_fetch_array($res1))
                            { 
                            //$cnt++; 
                           
                    ?>
                  <li>
                    <a href="read-mail.php?name=<?php echo $row1['mid'];?>">
                      <h4>
                        <?php echo $row1['mname'];?>
                        <small><i class="fa fa-clock-o"></i></small>
                      </h4>
                      <p><?php echo $row1['mmessage']; ?></p>
                    </a>
                  </li>
                                      <?php  
                            } 
                        ?>
                </ul>
              </li>
              <li class="footer"><a href="mailbox.php">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
            
                   
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"></span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 0 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">0</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have No Task</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <!-- end task item -->
                    <!-- Task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
            
          <li class="dropdown user user-menu">
            <a href="index.php" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../../images/userdefault.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Admin</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../../images/userdefault.png" class="img-circle" alt="User Image">

                <p>
                  Admin - 
<!--                  <small>Member since Nov. 2012</small>-->
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
               <form action="" method="post">
                  <div class="pull-right" style="margin-right: 80px;">
                      <input type="submit" class="btn btn-primary" name="out" value="Sign Out">
                </div>
                </form>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
<!--
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
-->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../../images/userdefault.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo "Kitchen Wonders"; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="post" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="../../index.php">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i>
            <span>Add Product</span>
            <span class="pull-right-container">
            
            <small class="label pull-right bg-green">new</small></span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../../addhomeappli.php"><i class="fa fa-circle-o"></i>Home Appliance</a></li>
            <li><a href="../../kitchenappli.php"><i class="fa fa-circle-o"></i>Kitchen Appliance</a></li>
            <li><a href="../../kitchentools.php"><i class="fa fa-circle-o"></i>Kitchen Tools</a></li>
            <li><a href="../../sliderimage.php"><i class="fa fa-circle-o"></i>Image Home Slider</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="../../paymentdetails.php">
            <i class="fa fa-money"></i>
            <span>Transaction Details</span>
          </a>
        </li>
        <li class="treeview">
          <a href="../../tables.php">
            <i class="fa fa-th"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-pencil"></i>
            </span>
          </a>
        </li>
        <li class="active treeview">
          <a href="mailbox.php">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Read Mail
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Mailbox</li>
      </ol>
    </section>
<?php 
      
      $str="SELECT * FROM message WHERE mid =".$_GET['name'];
      $res=mysqli_query($conn,$str);
      if($row=mysqli_fetch_array($res))
      {
      ?>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          <a href="mailbox.php" class="btn btn-primary btn-block margin-bottom">Inbox</a>

          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="mailbox.php"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right"><?php echo $cnt; ?></span></a></li>
                
<!--                <li><a href="#"><i class="fa fa-trash-o"></i> Trash</a></li>-->
              </ul>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Read Mail</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="mailbox-read-info">
                <h3><?php echo $row['subject']; ?></h3>
                <h5>From:<?php echo $row['mmail'];?>
                  <span class="mailbox-read-time pull-right"><?php echo $row['mdate'];?></span></h5>
              </div>
              <!-- /.mailbox-read-info -->
              <div class="mailbox-controls with-border text-center">
                   City:<?php echo $row['mcity'];?>
                  <span class="mailbox-read-time pull-right"> Contact No : <?php echo $row['mphone'];?></span>
                <!-- /.btn-group -->
              </div>
              <!-- /.mailbox-controls -->
              <div class="mailbox-read-message">
                  <br><br>
                <p style="margin-left: 40px;
margin-right:40px;">&nbsp;&nbsp;&nbsp;<?php
                    
                        echo $row['mmessage'];
                    
                    ?></p>
                <p>Thanks,<br></p>
              </div>
                <div class="mailbox-controls with-border text-center">
                    <div class="btn-group">
                  <button type="button" name="delete" class="btn btn-default btn-sm delete-id" id="delete" data-toggle="tooltip" data-container="body" title="Delete" data-delete-id="<?php echo $row['mid']; ?>">
                    <i class="fa fa-trash-o"></i></button>
                </div>
                </div>
              <!-- /.mailbox-read-message -->
            </div>
            <!-- /.box-body -->
            <!-- /.box-footer -->
          </div>
          <!-- /. box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
      <?php
      
          }
      
      ?>
    <!-- /.content -->
  </div>
    
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
<!--      <b>Version</b> 2.3.6-->
    </div>
    <strong>Copyright &copy; 2017-2018 <a href="http://Hiveslab.com" target="_blank">Hives Online India Pvt.Ltd</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<script>
$(document).on('click','.delete-id',function()
 {
    var did = $(this).data('delete-id');
    if(confirm("Are You Sure..?"))
    {    
    $.ajax({
        type:'post',
        url:'deletemsg.php',
        data:{'deleteId':did},
        success:function(data){
            alert(data);
            window.location.href='mailbox.php';
        }
    })
    }
});
    
</script>    
</body>
</html>
<?php
if (isset($_POST['out']))
{
    session_destroy();
    echo"<script>window.location.href='../../../index.php';</script>";
}
?>